# Generated by Django 3.2 on 2021-04-24 21:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Arcticles',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_group', models.CharField(max_length=50, verbose_name='Номер группы ')),
                ('special', models.CharField(max_length=100, verbose_name='Специальность ')),
            ],
            options={
                'verbose_name_plural': 'Группы',
            },
        ),
        migrations.CreateModel(
            name='Arcticles_Students',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fullname', models.CharField(max_length=50, verbose_name='Фамилия ')),
                ('name', models.CharField(max_length=100, verbose_name='Имя ')),
                ('otchest', models.CharField(max_length=100, verbose_name='Отчество ')),
                ('category1', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='MainPage.arcticles', verbose_name='Группа')),
            ],
            options={
                'verbose_name_plural': 'Студенты',
            },
        ),
    ]
