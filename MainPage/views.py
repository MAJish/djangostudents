from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.contrib.auth.models import User
from .models import Arcticles, Arcticles_Students, _get_groups
from .forms import ArcticlesForm, Arcticles_Form_Students
from django.views.generic import UpdateView, DetailView, DeleteView
from django.shortcuts import render, redirect


class Newsupdate_Students(UpdateView):
    model = Arcticles_Students
    template_name = 'myPage1/create_students.html'
    form_class = Arcticles_Form_Students


class Newsupdate_groups(UpdateView):
    model = Arcticles
    template_name = 'myPage1/create_groups.html'
    form_class = ArcticlesForm


class Delete_students(DeleteView):
    model = Arcticles_Students
    template_name = 'myPage1/delete_students.html'
    success_url = '/students/'


class Delete_groups(DeleteView):
    model = Arcticles
    template_name = 'myPage1/delete_groups.html'
    success_url = '/groups/'


def search_groups(request):
    name = Arcticles.objects.all()
    _get_groups()
    return render(request, 'myPage1/groups.html', {'name': name,
                                                   'count_students': count_students_in_groups(request)})


def students_in_groups(request, pk):
    id_group = Arcticles.objects.filter(id=pk);
    students = Arcticles_Students.objects.all()
    c=[]
    for i in students:
        if (id_group[0] == i.category1):
            c.append(i)
    return render(request, 'myPage1/find_students_in_groups.html',
                  {'answer': c} )


def count_students_in_groups(request):
    groups = _get_groups()
    c = []
    c2=[]
    student = Arcticles_Students.objects.all()
    for i in student:
        c.append(i.category1)
    v=list(c)
    for y in groups:
        try:
            n = 0
            c1 = []
            for i in v:
                if (i.number_group == y[0]):
                    n = n + 1
            c1.append(y[0])
            c1.append(n)
            c2.append(c1)
        except:
            c1.append(y[0])
            c1.append("0")
            c2.append(c1)
    return tuple(c2)


def search_students(request):
    name = Arcticles_Students.objects.all()
    return render(request, 'myPage1/students.html', {'name': name})


def create_groups(request):
    error = ''
    if request.method == 'POST':
        form = ArcticlesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('FirstTry:groups')
        else:
            error = " Форма неверна заполнена"
    form = ArcticlesForm()
    data = {
        'form': form,
        'error': error
    }

    return render(request, 'myPage1/create_groups.html', data)


def create_students(request):
    error = ''
    if request.method == 'POST':
        form = Arcticles_Form_Students(request.POST)
        if form.is_valid():
            form.save()
            return redirect('FirstTry:students')
        else:
            error = " Форма неверна заполнена"
    form = Arcticles_Form_Students()
    data = {
        'form': form,
        'error': error
    }
    return render(request, 'myPage1/create_students.html', data)
