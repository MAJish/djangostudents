from django.utils.regex_helper import Choice

from .models import Arcticles, Arcticles_Students, _get_groups
from django.forms import ModelForm, TextInput, SelectDateWidget, DateField, fields, Select, ChoiceField


class ArcticlesForm(ModelForm):
    class Meta:
        model = Arcticles
        fields = ['number_group', 'special', ]

        widgets = {
            "number_group": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Номер группы'
            }),
            "special": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Специальность',

            }),

        }


class Arcticles_Form_Students(ModelForm):
    class Meta:
        model = Arcticles_Students
        fields = ['fullname', 'name', 'otchest', 'category1']
        widgets = {
            "fullname": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Фамилия'
            }),
            "name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Имя'
            }),
            "otchest": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Отчество'
            }),
            "category1": Select(attrs={
                'class': 'form-control',
            }),
        }
