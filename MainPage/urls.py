from sys import path

from django.conf.urls import url
from . import views

app_name = 'FirstTry'
urlpatterns = [
    url('create_students/', views.create_students, name="create_students"),
    url(r'students_in_groups/(?P<pk>\d+)/', views.students_in_groups, name="students_in_groups"),
    url(r'students/(?P<pk>\d+)/', views.Newsupdate_Students.as_view(), name="studente"),
    url(r'students_delete/(?P<pk>\d+)/', views.Delete_students.as_view(), name="studente_delete"),
    url(r'groups_delete/(?P<pk>\d+)/', views.Delete_groups.as_view(), name="groups_delete"),
    url(r'groups/(?P<pk>\d+)/', views.Newsupdate_groups.as_view(), name="groups_up"),
    url(r'students', views.search_students, name="students"),
    url('create_groups/', views.create_groups, name="create"),
    url(r'groups/', views.search_groups, name="groups"),
    url(r'', views.search_groups, name="groups"),
]
