import form as form
from django.db import models

def _get_groups() -> tuple:
    name = Arcticles.objects.all()
    a = []
    for i in range((len(name))):
        b = []
        b.append(str(name[i]))
        b.append(str(name[i]))
        a.append(b)
    return tuple(a)


class Arcticles(models.Model):
    number_group = models.CharField('Номер группы ', max_length=50)
    special = models.CharField('Специальность ', max_length=100)

    def __str__(self):
        return self.number_group

    def get_absolute_url(self):
        return f'/groups/'

    class Meta:
        verbose_name_plural = 'Группы'


class Arcticles_Students(models.Model):
    fullname = models.CharField('Фамилия ', max_length=50)
    name = models.CharField('Имя ', max_length=100)
    otchest = models.CharField('Отчество ', max_length=100)
    category1 = models.ForeignKey(Arcticles, verbose_name="Группа", on_delete=models.SET_NULL, null=True)


    def __str__(self):
        return self.fullname + " " + self.name + " " + self.otchest

    def get_absolute_url(self):
        return f'/students/'


    class Meta:
        verbose_name_plural = 'Студенты'


